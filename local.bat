rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t dial-creator-windows --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name dial-creator-windows dial-creator-windows
docker cp dial-creator-windows:/game/built built
docker rm dial-creator-windows
cd built
dial-creator.exe
