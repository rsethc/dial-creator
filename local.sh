#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t dial-creator-ubuntu --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
docker create --name dial-creator-ubuntu dial-creator-ubuntu
docker cp dial-creator-ubuntu:/game/built built
docker rm dial-creator-ubuntu
cd built
./dial-creator
