#include <slice.h>
SDL_Color color1 = {255,0,0,255};
SDL_Color color2 = {0,0,255,255};
SDL_Color backcolor = {0,0,0,0};
slScalar centerx = 0.5;
slScalar centery = 0.5;
slScalar radiusinner = 0.4;
slScalar radiusouter = 0.425;
slScalar angstart = slDegToRad(180);
slScalar angend = slDegToRad(0);
slBU resolution = 64;
SDL_Color CalculateColor (slScalar x, slScalar y)
{
	// Give coordinates relative to the center of circle, not top-left corner of image.
	slScalar dist = pow(x * x + y * y,0.5);
	if (dist < radiusinner || dist > radiusouter) return backcolor;
	slScalar ang = atan2(y,x);
	if (ang < 0) ang += M_PI * 2;
	if (angstart < angend)
	{
		if (ang < angstart || ang > angend) return backcolor;
	}
	else if (ang > angstart || ang < angend) return backcolor;
	slScalar value = (ang - angstart) / (angend - angstart);
	slScalar inverse = 1 - value;
	return {
		inverse * color1.r + value * color2.r,
		inverse * color1.g + value * color2.g,
		inverse * color1.b + value * color2.b,
		inverse * color1.a + value * color2.a
	};
};
void ShadeRow (slBU y, slBU end, SDL_Color* pixels)
{
	pixels += y * resolution;
	for (; y < end; y++)
	{
		slScalar yvalue = (((resolution - 1) - y) / (slScalar)(resolution - 1)) - centery;
		for (slBU x = 0; x < resolution; x++)
			*pixels++ = CalculateColor((x / (slScalar)(resolution - 1)) - centerx,yvalue);
	};
};
slBox* BackBox;
SDL_Color* pixels;
void RedrawBack ()
{
	slDoSimpleWork(resolution,ShadeRow,pixels);
	BackBox->SetTexRef(slCreateCustomTexture(resolution,resolution,pixels));
};
slSlider* slider_r1;
slSlider* slider_g1;
slSlider* slider_b1;
slSlider* slider_a1;
slSlider* slider_deg1;
slSlider* slider_r2;
slSlider* slider_g2;
slSlider* slider_b2;
slSlider* slider_a2;
slSlider* slider_deg2;
slSlider* slider_res;
slSlider* slider_inner;
slSlider* slider_outer;
bool slider_deg1_lock = true;
bool slider_deg2_lock = true;
void UpdateDegreesText (slSlider* slider)
{
	char* text;
	asprintf(&text,"%u Degress",(unsigned int)slRound(slider->curvalue));
	slider->mark->SetTexRef(slRenderText(text));
	free(text);
};
void UpdateRadiusText (slSlider* slider)
{
	char* text;
	asprintf(&text,"%.3f",(float)slider->curvalue);
	slider->mark->SetTexRef(slRenderText(text));
	free(text);
};
void UpdateResText (slSlider* slider)
{
	char* text;
	asprintf(&text,"%ux%u",(unsigned int)resolution,(unsigned int)resolution);
	slider->mark->SetTexRef(slRenderText(text));
	free(text);
};
void OnColorModify (slSlider* slider)
{
	Uint8* byte = slider->userdata;
	*byte = slider->curvalue;
	RedrawBack();
};
void OnAlphaModify (slSlider* slider)
{
	Uint8* byte = slider->userdata;
	*byte = slider->curvalue;
	slider->mark->backcolor.a = *byte;
	slider->mark->hoverbackcolor.a = *byte;
	RedrawBack();
};
void OnDegreesModify (slSlider* slider)
{
	slScalar* angle = slider->userdata;
	*angle = slDegToRad(slider->curvalue);
	UpdateDegreesText(slider);
	RedrawBack();
};
void OnInnerModify ()
{
	radiusinner = slider_inner->curvalue;
	UpdateRadiusText(slider_inner);
	if (radiusouter < radiusinner)
	{
		radiusouter = radiusinner;
		slider_outer->SetValue(radiusouter);
		UpdateRadiusText(slider_outer);
	};
	RedrawBack();
};
void OnOuterModify ()
{
	radiusouter = slider_outer->curvalue;
	UpdateRadiusText(slider_outer);
	if (radiusouter < radiusinner)
	{
		radiusinner = radiusouter;
		slider_inner->SetValue(radiusinner);
		UpdateRadiusText(slider_inner);
	};
	RedrawBack();
};
void OnResModify ()
{
	resolution = pow(2,slRound(slider_res->curvalue));
	pixels = realloc(pixels,resolution * resolution * 4);
	UpdateResText(slider_res);
	RedrawBack();
};
slBox* slider_deg1_lock_button;
slBox* slider_deg2_lock_button;
void OnDegLockModify (slBox* box)
{
	bool state;
	if (box == slider_deg1_lock_button)
	{
		slider_deg1_lock = !slider_deg1_lock;
		state = slider_deg1_lock;
		if (state) OnDegreesModify(slider_deg1);
		else UpdateDegreesText(slider_deg1);
	}
	else
	{
		slider_deg2_lock = !slider_deg2_lock;
		state = slider_deg2_lock;
		if (state) OnDegreesModify(slider_deg2);
		else UpdateDegreesText(slider_deg2);
	};
	box->SetTexRef(slRenderText(state ? "Disable Integer Snap" : "Enable Integer Snap"));
};
slBox* c1info;
slBox* c2info;
slBox* radiusinfo;
slBox* resinfo;
slBox* IntSnapButton (bool currently_locked)
{
	slBox* button = slCreateBox(slRenderText(currently_locked ? "Disable Integer Snap" : "Enable Integer Snap"));
	button->bordercolor = {255,255,255,255};
	button->onclick = OnDegLockModify;
	button->hoverable = true;
	button->hoverbordercolor = {255,255,255,255};
	button->hoverbackcolor = {63,63,63,255};
	return button;
};
slSlider* GeneralSlider (slScalar y, bool is_bar)
{
	slBox* back = slCreateBox();
	slSetBoxDims(back,slVec2(0.05,y),slVec2(0.4,0.04),100);
	back->bordercolor = {255,255,255,255};

	slBox* mark = slCreateBox();
	mark->bordercolor = {63,63,63,255};
	mark->hoverable = true;
	mark->hoverbordercolor = {191,191,191,255};
	slSetBoxDims(mark,slVec2(0.05,y + 0.005),slVec2(0.125,0.03),101);
	mark->hoverbackcolor = {63,63,63,255};

	slSlider* slider = slCreateSlider(back,mark,false,is_bar,false);
	return slider;
};
slSlider* ColorSlider (slScalar y, SDL_Color color, Uint8* valueptr)
{
	slSlider* slider = GeneralSlider(y,true);
	slider->onchange = OnColorModify;
	slider->userdata = valueptr;
	slider->mark->backcolor = color;
	slider->mark->hoverbackcolor = color;
	slider->minvalue = 0.5;
	slider->maxvalue = 255.5;
	slider->SetValue(*valueptr + 0.5);
	return slider;
};
slSlider* AlphaSlider (slScalar y, Uint8* valueptr)
{
	slSlider* slider = ColorSlider(y,{255,255,255,*valueptr},valueptr);
	slider->onchange = OnAlphaModify;
    return slider;
};
slSlider* DegreesSlider (slScalar y, slScalar* valueptr)
{
	slSlider* slider = GeneralSlider(y,false);
	slider->onchange = OnDegreesModify;
	slider->userdata = valueptr;
	slider->minvalue = 0;
	slider->maxvalue = 360;
	slider->SetValue(slRadToDeg(*valueptr));
	UpdateDegreesText(slider);
	return slider;
};
slSlider* RadiusSlider (slScalar y, slScalar initial_value, void (*on_modify) ())
{
	slSlider* slider = GeneralSlider(y,false);
	slider->onchange = on_modify;
	slider->minvalue = 0;
	slider->maxvalue = 0.5;
	slider->SetValue(initial_value);
	UpdateRadiusText(slider);
	return slider;
};
slSlider* ResSlider (slScalar y)
{
	slSlider* slider = GeneralSlider(y,false);
	slider->onchange = OnResModify;
	slider->minvalue = 4;
	slider->maxvalue = 10;
	slider->SetValue(log2(resolution));
	UpdateResText(slider);
	return slider;
};
void InitModifyUI ()
{
	slider_deg1_lock_button = IntSnapButton(slider_deg1_lock);
	slSetBoxDims(slider_deg1_lock_button,0.125,0.29,0.25,0.03,100);

	slider_deg2_lock_button = IntSnapButton(slider_deg2_lock);
	slSetBoxDims(slider_deg2_lock_button,0.125,0.59,0.25,0.03,100);

	c1info = slCreateBox(slRenderText("Color 1"));
	c2info = slCreateBox(slRenderText("Color 2"));
	radiusinfo = slCreateBox(slRenderText("Inside & Outside Radii"));
	resinfo = slCreateBox(slRenderText("Image Resolution"));
	slSetBoxDims(c1info,0,0.06,0.5,0.03,100);
	slSetBoxDims(c2info,0,0.36,0.5,0.03,100);
	slSetBoxDims(radiusinfo,0,0.77,0.5,0.03,100);
	slSetBoxDims(resinfo,0,0.67,0.5,0.03,100);



	slider_r1 = ColorSlider(0.09,{255,0,0,255},&color1.r);
	slider_g1 = ColorSlider(0.13,{0,255,0,255},&color1.g);
	slider_b1 = ColorSlider(0.17,{0,0,255,255},&color1.b);
	slider_a1 = AlphaSlider(0.21,&color1.a);
	slider_deg1 = DegreesSlider(0.25,&angstart);

	slider_r2 = ColorSlider(0.39,{255,0,0,255},&color2.r);
	slider_g2 = ColorSlider(0.43,{0,255,0,255},&color2.g);
	slider_b2 = ColorSlider(0.47,{0,0,255,255},&color2.b);
	slider_a2 = AlphaSlider(0.51,&color2.a);
	slider_deg2 = DegreesSlider(0.55,&angend);

	slider_res = ResSlider(0.70);

	slider_inner = RadiusSlider(0.80,radiusinner,OnInnerModify);
	slider_outer = RadiusSlider(0.84,radiusouter,OnOuterModify);
};
void QuitModifyUI ()
{
	slDestroyBox(slider_deg1_lock_button);
	slDestroyBox(slider_deg2_lock_button);
	slDestroySlider(slider_r1);
	slDestroySlider(slider_g1);
	slDestroySlider(slider_b1);
	slDestroySlider(slider_a1);
	slDestroySlider(slider_deg1);
	slDestroySlider(slider_r2);
	slDestroySlider(slider_g2);
	slDestroySlider(slider_b2);
	slDestroySlider(slider_a2);
	slDestroySlider(slider_deg2);
	slDestroySlider(slider_res);
	slDestroyBox(c1info);
	slDestroyBox(c2info);
	slDestroyBox(radiusinfo);
	slDestroyBox(resinfo);
};
bool FilePathSubmitted;
void SubmitFilePath ()
{
	FilePathSubmitted = true;
};
bool SaveCanceled;
void CancelSave () { SaveCanceled = true; };
void SaveImage ()
{
	SDL_Surface* surf = SDL_CreateRGBSurfaceFrom(pixels,resolution,resolution,32,resolution * 4,0x000000FF,0x0000FF00,0x00FF0000,0xFF000000);
	slBox* backbox = slCreateBox();
	backbox->bordercolor = {255,255,255,255};
	backbox->backcolor = {0,0,0,255};
	backbox->onclick = slDoNothing;
	slSetBoxDims(backbox,0.05,0.35,0.9,0.3,90);
	slBox* info = slCreateBox(slRenderText("Enter File Path (excluding \".png\")"));
	slSetBoxDims(info,0.1,0.4,0.8,0.05,80);
	slBox* tbb = slCreateBox();
	slSetBoxDims(tbb,0.1,0.45,0.8,0.1,80);
	tbb->bordercolor = {255,255,255,255};
	tbb->hoverable = true;
	tbb->hoverbordercolor = {255,255,255,255};
	tbb->hoverbackcolor = {63,63,63,255};
	slTypingBox* pathbox = slCreateTypingBox(tbb);
    pathbox->Focus();
	pathbox->onenter = SubmitFilePath;
	slBox* submitbox = slCreateBox(slRenderText("Submit"));
	slSetBoxDims(submitbox,0.295,0.55,0.2,0.05,80);
	submitbox->bordercolor = {255,255,255,255};
	submitbox->hoverbordercolor = {255,255,255,255};
	submitbox->hoverbackcolor = {63,63,63,255};
	submitbox->hoverable = true;
	submitbox->onclick = SubmitFilePath;
	FilePathSubmitted = false;
	slBox* cancelbutton = slCreateBox(slRenderText("Cancel"));
	slSetBoxDims(cancelbutton,0.505,0.55,0.2,0.05,80);
	cancelbutton->bordercolor = {255,255,255,255};
	cancelbutton->hoverbordercolor = {255,255,255,255};
	cancelbutton->hoverbackcolor = {63,63,63,255};
	cancelbutton->hoverable = true;
	cancelbutton->onclick = CancelSave;
	SaveCanceled = false;
	while (!FilePathSubmitted)
	{
		slCycle();
		if (slGetReqt() || SaveCanceled) goto SKIPSAVE;
	};
    {
    char* typed = pathbox->GenText();
	char* path = slStrConcat(typed,".png");
    free(typed);
	IMG_SavePNG(surf,path);
	free(path);
    }
	SKIPSAVE:
	SDL_FreeSurface(surf);
	slDestroyTypingBox(pathbox);
	slDestroyBox(info);
	slDestroyBox(submitbox);
	slDestroyBox(backbox);
	slDestroyBox(cancelbutton);
};
#include <slicefps.h>
int main ()
{
	slInit("Dial Creation Tool");
	fpsInit();
	pixels = malloc(resolution * resolution * 4);

	BackBox = slCreateBox();
	slSetBoxDims(BackBox,0.5,0.25,0.5,0.5,100);
	BackBox->bordercolor = {255,255,255,255};
	BackBox->maintainaspect = true;
	slBox* previewinfo = slCreateBox(slRenderText("Image Preview"));
	slSetBoxDims(previewinfo,0.5,0.22,0.5,0.03,100);
	previewinfo->maintainaspect = true;
	slBox* SaveButton = slCreateBox(slRenderText("Save Image"));
	SaveButton->bordercolor = {255,255,255,255};
	slSetBoxDims(SaveButton,0.625,0.8,0.25,0.06,100);
	SaveButton->onclick = SaveImage;
	SaveButton->hoverable = true;
	SaveButton->hoverbordercolor = {255,255,255,255};
	SaveButton->hoverbackcolor = {63,63,63,255};
	SaveButton->maintainaspect = true;

	InitModifyUI();
	RedrawBack();
	while (!slGetReqt()) slCycle();
	free(pixels);
	slDestroyBox(BackBox);
	slDestroyBox(previewinfo);
	slDestroyBox(SaveButton);
	QuitModifyUI();
	slQuit();
};
