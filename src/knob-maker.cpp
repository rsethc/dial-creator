#include <slice.h>
SDL_Color color1 = {255,0,0,255};
SDL_Color color2 = {0,0,255,255};
SDL_Color backcolor = {0,0,0,0};
slScalar centerx = 0.5;
slScalar centery = 0.5;
slScalar radiusinner = 0.4;
slScalar radiusouter = 0.425;
slScalar angstart = slDegToRad(180);
slScalar angend = slDegToRad(0);
slScalar resolution = 64;

SDL_Color knobcolor = {0,0,0,255};
SDL_Color dotcolor = {255,255,255,255};
slScalar button_radius = 0.4;
slScalar dot_radius = 0.02;
slScalar dot_inwardness = 0.04;
SDL_Color CalculateColor (slScalar x, slScalar y)
{
	// Give coordinates relative to the center of circle, not top-left corner of image.
	slScalar dist = pow(x * x + y * y,0.5);
	if (dist > button_radius) return backcolor;
	slScalar dotx = x - (button_radius - dot_inwardness);
	dist = pow(dotx * dotx + y * y,0.5);
	if (dist > dot_radius) return knobcolor;
	return dotcolor;
};/*



	if (dist < radiusinner || dist > radiusouter) return backcolor;
	slScalar ang = atan2(y,x);
	if (ang < 0) ang += M_PI * 2;
	if (angstart < angend)
	{
		if (ang < angstart || ang > angend) return backcolor;
	}
	else if (ang > angstart || ang < angend) return backcolor;
	slScalar value = (ang - angstart) / (angend - angstart);
	slScalar inverse = 1 - value;
	return {
		inverse * color1.r + value * color2.r,
		inverse * color1.g + value * color2.g,
		inverse * color1.b + value * color2.b,
		inverse * color1.a + value * color2.a
	};
};*/
void ShadeMap (Uint8* pixels, slBU res)
{
	for (slBU y = 0; y < res; y++)
	{
		slScalar yvalue = (((res - 1) - y) / (slScalar)(res - 1)) - centery;
		for (slBU x = 0; x < res; x++)
		{
			SDL_Color color = CalculateColor((x / (slScalar)(res - 1)) - centerx,yvalue);
			*pixels = color.r;
			pixels++;
			*pixels = color.g;
			pixels++;
			*pixels = color.b;
			pixels++;
			*pixels = color.a;
			pixels++;
		};
	};
};
void RGBAtoABGR (Uint8* pixels, slBU pixelcount, Uint8* outbuf)
{
    for (slBU cur = 0; cur < pixelcount; cur++, pixels += 4, outbuf += 4)
    {
        Uint8 r = *pixels;
        Uint8 g = *(pixels + 1);
        Uint8 b = *(pixels + 2);
        Uint8 a = *(pixels + 3);
        *(outbuf + 3) = r;
        *(outbuf + 2) = g;
        *(outbuf + 1) = b;
        *outbuf = a;
    };
};
slBox* BackBox;
Uint8* pixels;
void RedrawBack ()
{
	ShadeMap(pixels,resolution);
	RGBAtoABGR(pixels,resolution * resolution,pixels);
	BackBox->texref = slCreateCustomTexture(resolution,resolution,pixels,true);
};
slSlider* slider_r1;
slSlider* slider_g1;
slSlider* slider_b1;
slSlider* slider_a1;
slSlider* slider_deg1;
slSlider* slider_r2;
slSlider* slider_g2;
slSlider* slider_b2;
slSlider* slider_a2;
slSlider* slider_deg2;
slSlider* slider_res;
slSlider* slider_inner;
slSlider* slider_outer;
bool slider_deg1_lock = true;
bool slider_deg2_lock = true;
void UpdateSliderText (slSlider* slider)
{
	char* text;
	if (slider == slider_deg1)
	{
		if (slider_deg1_lock) asprintf(&text,"%u Degress",(unsigned int)slRound(slider->curvalue));
		else asprintf(&text,"%.3f Degrees",(float)slider->curvalue);
	}
	else if (slider == slider_deg2)
	{
		if (slider_deg2_lock) asprintf(&text,"%u Degress",(unsigned int)slRound(slider->curvalue));
		else asprintf(&text,"%.3f Degrees",(float)slider->curvalue);
	}
	else if (slider == slider_res) asprintf(&text,"%ux%u",(unsigned int)resolution,(unsigned int)resolution);
	else if (slider == slider_inner || slider == slider_outer) asprintf(&text,"%.3f",(float)slider->curvalue);
	slider->mark->texref = slRenderText(text);
	free(text);
};
void OnSliderModify (slSlider* slider)
{
	char* text;
	if (slider == slider_r1) color1.r = slider->curvalue;
	else if (slider == slider_g1) color1.g = slider->curvalue;
	else if (slider == slider_b1) color1.b = slider->curvalue;
	else if (slider == slider_a1)
	{
		color1.a = slider->curvalue;
		slider->mark->backcolor.a = slider->curvalue;
		slider->mark->hoverbackcolor.a = slider->curvalue;
	}
	else if (slider == slider_deg1)
	{
		if (slider_deg1_lock) slSetSliderValue(slider,slRound(slider->curvalue));
		angstart = slDegToRad(slider->curvalue);
		UpdateSliderText(slider);
	}
	else if (slider == slider_r2) color2.r = slider->curvalue;
	else if (slider == slider_g2) color2.g = slider->curvalue;
	else if (slider == slider_b2) color2.b = slider->curvalue;
	else if (slider == slider_a2)
	{
		color2.a = slider->curvalue;
		slider->mark->backcolor.a = slider->curvalue;
		slider->mark->hoverbackcolor.a = slider->curvalue;
	}
	else if (slider == slider_deg2)
	{
		if (slider_deg2_lock) slSetSliderValue(slider,slRound(slider->curvalue));
		angend = slDegToRad(slider->curvalue);
		UpdateSliderText(slider);
	}
	else if (slider == slider_res)
	{
		resolution = pow(2,slRound(slider->curvalue));
		pixels = realloc(pixels,resolution * resolution * 4);
		UpdateSliderText(slider);
	}
	else if (slider == slider_inner)
	{
		radiusinner = slider->curvalue;
		UpdateSliderText(slider);
		if (radiusouter < radiusinner)
		{
			radiusouter = radiusinner;
			slSetSliderValue(slider_outer,radiusouter);
			UpdateSliderText(slider_outer);
		};
	}
	else if (slider == slider_outer)
	{
		radiusouter = slider->curvalue;
		UpdateSliderText(slider);
		if (radiusouter < radiusinner)
		{
			radiusinner = radiusouter;
			slSetSliderValue(slider_inner,radiusinner);
			UpdateSliderText(slider_inner);
		};
	};
	RedrawBack();
};
slBox* slider_deg1_lock_button;
slBox* slider_deg2_lock_button;
void OnDegLockModify (slBox* box)
{
	bool state;
	if (box == slider_deg1_lock_button)
	{
		slider_deg1_lock = !slider_deg1_lock;
		state = slider_deg1_lock;
		if (state) OnSliderModify(slider_deg1);
		else UpdateSliderText(slider_deg1);
	}
	else
	{
		slider_deg2_lock = !slider_deg2_lock;
		state = slider_deg2_lock;
		if (state) OnSliderModify(slider_deg2);
		else UpdateSliderText(slider_deg2);
	};
	box->texref = slRenderText(state ? "Disable Integer Snap" : "Enable Integer Snap");
};
slBox* c1info;
slBox* c2info;
slBox* radiusinfo;
slBox* resinfo;
void InitModifyUI ()
{
	slider_deg1_lock_button = slCreateBox();
	slider_deg1_lock_button->bordercolor = {255,255,255,255};
	slider_deg1_lock_button->onclick = OnDegLockModify;
	slider_deg1_lock_button->texref = slRenderText(slider_deg1_lock ? "Disable Integer Snap" : "Enable Integer Snap");
	slSetBoxDims(slider_deg1_lock_button,0.125,0.29,0.25,0.03,100);
	slider_deg1_lock_button->hoverable = true;
	slider_deg1_lock_button->hoverbordercolor = {255,255,255,255};
	slider_deg1_lock_button->hoverbackcolor = {63,63,63,255};

	slider_deg2_lock_button = slCreateBox();
	slider_deg2_lock_button->bordercolor = {255,255,255,255};
	slider_deg2_lock_button->onclick = OnDegLockModify;
	slider_deg2_lock_button->texref = slRenderText(slider_deg2_lock ? "Disable Integer Snap" : "Enable Integer Snap");
	slSetBoxDims(slider_deg2_lock_button,0.125,0.59,0.25,0.03,100);
	slider_deg2_lock_button->hoverable = true;
	slider_deg2_lock_button->hoverbordercolor = {255,255,255,255};
	slider_deg2_lock_button->hoverbackcolor = {63,63,63,255};

	c1info = slCreateBox(slRenderText("Color 1"));
	c2info = slCreateBox(slRenderText("Color 2"));
	radiusinfo = slCreateBox(slRenderText("Inside & Outside Radii"));
	resinfo = slCreateBox(slRenderText("Image Resolution"));
	slSetBoxDims(c1info,0,0.06,0.5,0.03,100);
	slSetBoxDims(c2info,0,0.36,0.5,0.03,100);
	slSetBoxDims(radiusinfo,0,0.77,0.5,0.03,100);
	slSetBoxDims(resinfo,0,0.67,0.5,0.03,100);

	slBox* slider_r1_back = slCreateBox();
	slBox* slider_g1_back = slCreateBox();
	slBox* slider_b1_back = slCreateBox();
	slBox* slider_a1_back = slCreateBox();
	slBox* slider_deg1_back = slCreateBox();
	slBox* slider_r2_back = slCreateBox();
	slBox* slider_g2_back = slCreateBox();
	slBox* slider_b2_back = slCreateBox();
	slBox* slider_a2_back = slCreateBox();
	slBox* slider_deg2_back = slCreateBox();
	slBox* slider_res_back = slCreateBox();
	slBox* slider_inner_back = slCreateBox();
	slBox* slider_outer_back = slCreateBox();

	slSetBoxDims(slider_r1_back,0.05,0.09,0.4,0.04,100);
	slSetBoxDims(slider_g1_back,0.05,0.13,0.4,0.04,100);
	slSetBoxDims(slider_b1_back,0.05,0.17,0.4,0.04,100);
	slSetBoxDims(slider_a1_back,0.05,0.21,0.4,0.04,100);
	slSetBoxDims(slider_deg1_back,0.05,0.25,0.4,0.04,100);
	slSetBoxDims(slider_r2_back,0.05,0.39,0.4,0.04,100);
	slSetBoxDims(slider_g2_back,0.05,0.43,0.4,0.04,100);
	slSetBoxDims(slider_b2_back,0.05,0.47,0.4,0.04,100);
	slSetBoxDims(slider_a2_back,0.05,0.51,0.4,0.04,100);
	slSetBoxDims(slider_deg2_back,0.05,0.55,0.4,0.04,100);
	slSetBoxDims(slider_res_back,0.05,0.70,0.4,0.04,100);
	slSetBoxDims(slider_inner_back,0.05,0.80,0.4,0.04,100);
	slSetBoxDims(slider_outer_back,0.05,0.84,0.4,0.04,100);

	slider_r1_back->bordercolor = {255,255,255,255};
	slider_g1_back->bordercolor = {255,255,255,255};
	slider_b1_back->bordercolor = {255,255,255,255};
	slider_a1_back->bordercolor = {255,255,255,255};
	slider_deg1_back->bordercolor = {255,255,255,255};
	slider_r2_back->bordercolor = {255,255,255,255};
	slider_g2_back->bordercolor = {255,255,255,255};
	slider_b2_back->bordercolor = {255,255,255,255};
	slider_a2_back->bordercolor = {255,255,255,255};
	slider_deg2_back->bordercolor = {255,255,255,255};
	slider_res_back->bordercolor = {255,255,255,255};
	slider_inner_back->bordercolor = {255,255,255,255};
	slider_outer_back->bordercolor = {255,255,255,255};

	slBox* slider_r1_mark = slCreateBox();
	slBox* slider_g1_mark = slCreateBox();
	slBox* slider_b1_mark = slCreateBox();
	slBox* slider_a1_mark = slCreateBox();
	slBox* slider_deg1_mark = slCreateBox();
	slBox* slider_r2_mark = slCreateBox();
	slBox* slider_g2_mark = slCreateBox();
	slBox* slider_b2_mark = slCreateBox();
	slBox* slider_a2_mark = slCreateBox();
	slBox* slider_deg2_mark = slCreateBox();
	slBox* slider_res_mark = slCreateBox();
	slBox* slider_inner_mark = slCreateBox();
	slBox* slider_outer_mark = slCreateBox();

	slider_r1_mark->bordercolor = {63,63,63,255};
	slider_g1_mark->bordercolor = {63,63,63,255};
	slider_b1_mark->bordercolor = {63,63,63,255};
	slider_a1_mark->bordercolor = {63,63,63,255};
	slider_deg1_mark->bordercolor = {63,63,63,255};
	slider_r2_mark->bordercolor = {63,63,63,255};
	slider_g2_mark->bordercolor = {63,63,63,255};
	slider_b2_mark->bordercolor = {63,63,63,255};
	slider_a2_mark->bordercolor = {63,63,63,255};
	slider_deg2_mark->bordercolor = {63,63,63,255};
	slider_res_mark->bordercolor = {63,63,63,255};
	slider_inner_mark->bordercolor = {63,63,63,255};
	slider_outer_mark->bordercolor = {63,63,63,255};

	slider_r1_mark->hoverable = true;
	slider_g1_mark->hoverable = true;
	slider_b1_mark->hoverable = true;
	slider_a1_mark->hoverable = true;
	slider_deg1_mark->hoverable = true;
	slider_r2_mark->hoverable = true;
	slider_g2_mark->hoverable = true;
	slider_b2_mark->hoverable = true;
	slider_a2_mark->hoverable = true;
	slider_deg2_mark->hoverable = true;
	slider_res_mark->hoverable = true;
	slider_inner_mark->hoverable = true;
	slider_outer_mark->hoverable = true;

	slider_r1_mark->hoverbordercolor = {191,191,191,255};
	slider_g1_mark->hoverbordercolor = {191,191,191,255};
	slider_b1_mark->hoverbordercolor = {191,191,191,255};
	slider_a1_mark->hoverbordercolor = {191,191,191,255};
	slider_deg1_mark->hoverbordercolor = {191,191,191,255};
	slider_r2_mark->hoverbordercolor = {191,191,191,255};
	slider_g2_mark->hoverbordercolor = {191,191,191,255};
	slider_b2_mark->hoverbordercolor = {191,191,191,255};
	slider_a2_mark->hoverbordercolor = {191,191,191,255};
	slider_deg2_mark->hoverbordercolor = {191,191,191,255};
	slider_res_mark->hoverbordercolor = {191,191,191,255};
	slider_inner_mark->hoverbordercolor = {191,191,191,255};
	slider_outer_mark->hoverbordercolor = {191,191,191,255};

	slSetBoxDims(slider_r1_mark,0.05,0.095,0.4,0.03,99);
	slSetBoxDims(slider_g1_mark,0.05,0.135,0.4,0.03,99);
	slSetBoxDims(slider_b1_mark,0.05,0.175,0.4,0.03,99);
	slSetBoxDims(slider_a1_mark,0.05,0.215,0.4,0.03,99);
	slSetBoxDims(slider_deg1_mark,0.05,0.255,0.125,0.03,99);
	slSetBoxDims(slider_r2_mark,0.05,0.395,0.4,0.03,99);
	slSetBoxDims(slider_g2_mark,0.05,0.435,0.4,0.03,99);
	slSetBoxDims(slider_b2_mark,0.05,0.475,0.4,0.03,99);
	slSetBoxDims(slider_a2_mark,0.05,0.515,0.4,0.03,99);
	slSetBoxDims(slider_deg2_mark,0.05,0.555,0.125,0.03,99);
	slSetBoxDims(slider_res_mark,0.05,0.705,0.125,0.03,99);
	slSetBoxDims(slider_inner_mark,0.05,0.805,0.125,0.03,99);
	slSetBoxDims(slider_outer_mark,0.05,0.845,0.125,0.03,99);

	slider_r1_mark->backcolor = {255,0,0,255};
	slider_g1_mark->backcolor = {0,255,0,255};
	slider_b1_mark->backcolor = {0,0,255,255};
	slider_a1_mark->backcolor = {255,255,255,color1.a};
	slider_r2_mark->backcolor = {255,0,0,255};
	slider_g2_mark->backcolor = {0,255,0,255};
	slider_b2_mark->backcolor = {0,0,255,255};
	slider_a2_mark->backcolor = {255,255,255,color2.a};

	slider_r1_mark->hoverbackcolor = slider_r1_mark->backcolor;
	slider_g1_mark->hoverbackcolor = slider_g1_mark->backcolor;
	slider_b1_mark->hoverbackcolor = slider_b1_mark->backcolor;
	slider_a1_mark->hoverbackcolor = slider_a1_mark->backcolor;
	slider_deg1_mark->hoverbackcolor = {63,63,63,255};
	slider_r2_mark->hoverbackcolor = slider_r2_mark->backcolor;
	slider_g2_mark->hoverbackcolor = slider_g2_mark->backcolor;
	slider_b2_mark->hoverbackcolor = slider_b2_mark->backcolor;
	slider_a2_mark->hoverbackcolor = slider_a2_mark->backcolor;
	slider_deg2_mark->hoverbackcolor = {63,63,63,255};
	slider_res_mark->hoverbackcolor = {63,63,63,255};
	slider_inner_mark->hoverbackcolor = {63,63,63,255};
	slider_outer_mark->hoverbackcolor = {63,63,63,255};

	slider_r1 = slCreateSlider(slider_r1_back,slider_r1_mark,false,true,false);
	slider_g1 = slCreateSlider(slider_g1_back,slider_g1_mark,false,true,false);
	slider_b1 = slCreateSlider(slider_b1_back,slider_b1_mark,false,true,false);
	slider_a1 = slCreateSlider(slider_a1_back,slider_a1_mark,false,true,false);
	slider_deg1 = slCreateSlider(slider_deg1_back,slider_deg1_mark,false,false,false);
	slider_r2 = slCreateSlider(slider_r2_back,slider_r2_mark,false,true,false);
	slider_g2 = slCreateSlider(slider_g2_back,slider_g2_mark,false,true,false);
	slider_b2 = slCreateSlider(slider_b2_back,slider_b2_mark,false,true,false);
	slider_a2 = slCreateSlider(slider_a2_back,slider_a2_mark,false,true,false);
	slider_deg2 = slCreateSlider(slider_deg2_back,slider_deg2_mark,false,false,false);
	slider_res = slCreateSlider(slider_res_back,slider_res_mark,false,false,false);
	slider_inner = slCreateSlider(slider_inner_back,slider_inner_mark,false,false,false);
	slider_outer = slCreateSlider(slider_outer_back,slider_outer_mark,false,false,false);

	slider_r1->minvalue = 0.5;
	slider_r1->maxvalue = 255.5;
	slSetSliderValue(slider_r1,color1.r + 0.5);
	slider_g1->minvalue = 0.5;
	slider_g1->maxvalue = 255.5;
	slSetSliderValue(slider_g1,color1.g + 0.5);
	slider_b1->minvalue = 0.5;
	slider_b1->maxvalue = 255.5;
	slSetSliderValue(slider_b1,color1.b + 0.5);
	slider_a1->minvalue = 0.5;
	slider_a1->maxvalue = 255.5;
	slSetSliderValue(slider_a1,color1.a + 0.5);
	slider_deg1->minvalue = 0;
	slider_deg1->maxvalue = 360;
	slSetSliderValue(slider_deg1,slRadToDeg(angstart));
	slider_r2->minvalue = 0.5;
	slider_r2->maxvalue = 255.5;
	slSetSliderValue(slider_r2,color2.r + 0.5);
	slider_g2->minvalue = 0.5;
	slider_g2->maxvalue = 255.5;
	slSetSliderValue(slider_g2,color2.g + 0.5);
	slider_b2->minvalue = 0.5;
	slider_b2->maxvalue = 255.5;
	slSetSliderValue(slider_b2,color2.b + 0.5);
	slider_a2->minvalue = 0.5;
	slider_a2->maxvalue = 255.5;
	slSetSliderValue(slider_a2,color2.a + 0.5);
	slider_deg2->minvalue = 0;
	slider_deg2->maxvalue = 360;
	slSetSliderValue(slider_deg2,slRadToDeg(angend));
	slider_res->minvalue = 4;
	slider_res->maxvalue = 10;
	slSetSliderValue(slider_res,log2(resolution));
	slider_inner->minvalue = 0;
	slider_inner->maxvalue = 0.5;
	slSetSliderValue(slider_inner,radiusinner);
	slider_outer->minvalue = 0;
	slider_outer->maxvalue = 0.5;
	slSetSliderValue(slider_outer,radiusouter);

	UpdateSliderText(slider_deg1);
	UpdateSliderText(slider_deg2);
	UpdateSliderText(slider_res);
	UpdateSliderText(slider_inner);
	UpdateSliderText(slider_outer);

	slider_r1->onchange = OnSliderModify;
	slider_g1->onchange = OnSliderModify;
	slider_b1->onchange = OnSliderModify;
	slider_a1->onchange = OnSliderModify;
	slider_deg1->onchange = OnSliderModify;
	slider_r2->onchange = OnSliderModify;
	slider_g2->onchange = OnSliderModify;
	slider_b2->onchange = OnSliderModify;
	slider_a2->onchange = OnSliderModify;
	slider_deg2->onchange = OnSliderModify;
	slider_res->onchange = OnSliderModify;
	slider_inner->onchange = OnSliderModify;
	slider_outer->onchange = OnSliderModify;
};
void QuitModifyUI ()
{
	slDestroyBox(slider_deg1_lock_button);
	slDestroyBox(slider_deg2_lock_button);
	slDestroySlider(slider_r1);
	slDestroySlider(slider_g1);
	slDestroySlider(slider_b1);
	slDestroySlider(slider_a1);
	slDestroySlider(slider_deg1);
	slDestroySlider(slider_r2);
	slDestroySlider(slider_g2);
	slDestroySlider(slider_b2);
	slDestroySlider(slider_a2);
	slDestroySlider(slider_deg2);
	slDestroySlider(slider_res);
	slDestroyBox(c1info);
	slDestroyBox(c2info);
	slDestroyBox(radiusinfo);
	slDestroyBox(resinfo);
};
bool FilePathSubmitted;
void SubmitFilePath ()
{
	FilePathSubmitted = true;
};
void SaveImage ()
{
	SDL_Surface* surf = SDL_CreateRGBSurfaceFrom(pixels,resolution,resolution,32,resolution * 4,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	slBox* backbox = slCreateBox();
	backbox->bordercolor = {255,255,255,255};
	backbox->backcolor = {0,0,0,255};
	backbox->onclick = slDoNothing;
	slSetBoxDims(backbox,0.05,0.35,0.9,0.3,90);
	slBox* info = slCreateBox(slRenderText("Enter File Path (excluding \".png\")"));
	slSetBoxDims(info,0.1,0.4,0.8,0.05,80);
	slBox* tbb = slCreateBox();
	slSetBoxDims(tbb,0.1,0.45,0.8,0.1,80);
	tbb->bordercolor = {255,255,255,255};
	tbb->hoverable = true;
	tbb->hoverbordercolor = {255,255,255,255};
	tbb->hoverbackcolor = {63,63,63,255};
	slTypingBox* pathbox = slCreateTypingBox(tbb,true);
	pathbox->ontextsubmit = SubmitFilePath;
	slBox* submitbox = slCreateBox(slRenderText("Submit"));
	slSetBoxDims(submitbox,0.4,0.55,0.2,0.05,80);
	submitbox->bordercolor = {255,255,255,255};
	submitbox->hoverbordercolor = {255,255,255,255};
	submitbox->hoverbackcolor = {63,63,63,255};
	submitbox->hoverable = true;
	submitbox->onclick = SubmitFilePath;
	FilePathSubmitted = false;
	while (!FilePathSubmitted)
	{
		slCycle();
		if (slGetReqt()) goto SKIPSAVE;
	};
	char* path = slStrConcat(pathbox->text,".png");
	IMG_SavePNG(surf,path);
	free(path);
	SKIPSAVE:
	SDL_FreeSurface(surf);
	slDestroyTypingBox(pathbox);
	slDestroyBox(info);
	slDestroyBox(submitbox);
	slDestroyBox(backbox);
};
int main ()
{
	slInit();
	pixels = malloc(resolution * resolution * 4);

	BackBox = slCreateBox();
	slSetBoxDims(BackBox,0.5,0.25,0.5,0.5,100);
	BackBox->bordercolor = {255,255,255,255};
	BackBox->maintainaspect = true;
	slBox* previewinfo = slCreateBox(slRenderText("Image Preview"));
	slSetBoxDims(previewinfo,0.5,0.22,0.5,0.03,100);
	previewinfo->maintainaspect = true;
	slBox* SaveButton = slCreateBox(slRenderText("Save Image"));
	SaveButton->bordercolor = {255,255,255,255};
	slSetBoxDims(SaveButton,0.625,0.8,0.25,0.06,100);
	SaveButton->onclick = SaveImage;
	SaveButton->hoverable = true;
	SaveButton->hoverbordercolor = {255,255,255,255};
	SaveButton->hoverbackcolor = {63,63,63,255};
	SaveButton->maintainaspect = true;

	InitModifyUI();
	RedrawBack();
	while (!slGetReqt()) slCycle();
	free(pixels);
	slDestroyBox(BackBox);
	slDestroyBox(previewinfo);
	slDestroyBox(SaveButton);
	QuitModifyUI();
	slQuit();
};
